import { format } from 'date-fns';
import { inputDateFormat } from './constants';
import * as axios from 'axios';
import { API_URL } from './config';

const getHeroes = async function() {
  try {
    const response = await axios.get(`${API_URL}/heroes.json`);
    let data = parseList(response);
    console.log('Todo parece OK!');
    const heroes = data.map(h => {
      h.originDate = format(h.originDate, inputDateFormat);
      return h;
    });
    return heroes;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const parseList = response => {
  let list = response.data;
  if (response.status != 200) throw new Error(response.message);
  if (!response.data) return [];
  if (typeof list != 'object') list = [];
  return list;
};

export const data = {
  getHeroes,
};
